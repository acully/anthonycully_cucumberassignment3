package com.ait.example;

public class Showlevel {

    private String description;

    private int requiredNumberOfTicketsBoughtLastSeason;

    public Showlevel(String description, int requiredNumberOfTicketsBoughtLastSeason) {
        this.description = description;
        this.requiredNumberOfTicketsBoughtLastSeason = requiredNumberOfTicketsBoughtLastSeason;
    }

    public String getDescription() {
        return description;
    }

    public int getRequiredNumberOfTicketsBoughtLastSeason() {
        return requiredNumberOfTicketsBoughtLastSeason;
    }
}
